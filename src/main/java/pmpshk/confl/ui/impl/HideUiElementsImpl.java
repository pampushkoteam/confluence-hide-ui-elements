package pmpshk.confl.ui.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import pmpshk.confl.ui.api.HideUiElements;

import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService({HideUiElements.class})
@Named("hideUiElements")
public class HideUiElementsImpl implements HideUiElements
{
	@ComponentImport
	private final ApplicationProperties applicationProperties;
	
	@Inject
	public HideUiElementsImpl(final ApplicationProperties applicationProperties)
	{
		this.applicationProperties = applicationProperties;
	}
	
	public String getName()
	{
		if (null != applicationProperties)
		{
			return "myComponent:" + applicationProperties.getDisplayName();
		}
		
		return "myComponent";
	}
}

package pmpshk.confl.ui.rest;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/user/permission")
public class UserPermRes
{
	private final UserAccessor userAccessor;
	
	@Autowired
	public UserPermRes(@ComponentImport UserAccessor userAccessor)
	{
		this.userAccessor = userAccessor;
	}
	
	@GET
	@AnonymousAllowed //An annotation to tell that resources are accessible by anonymous users
	@Produces({MediaType.APPLICATION_JSON})
	public Response getUserPermission()
	{
		boolean viewPageShareButtonPermission = false;
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		if (confluenceUser != null)
		{
			String username = confluenceUser.getName();
			//проверяем, находится ли он нужной нам группе
			boolean isMember = userAccessor.hasMembership("confluence-administrators", username);
			
			if (isMember)
			{
				viewPageShareButtonPermission = true;
			}
		}
		
		return Response.ok(new UserPermResModel(viewPageShareButtonPermission)).build();
	}
}

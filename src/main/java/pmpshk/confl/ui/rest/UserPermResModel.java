package pmpshk.confl.ui.rest;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "user_permission")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserPermResModel
{
	
	@XmlElement(name = "view_enabled")
	private Boolean viewPageShareButton;
	
	public UserPermResModel()
	{
	
	}
	
	public UserPermResModel(Boolean viewPageShareButton)
	{
		this.viewPageShareButton = viewPageShareButton;
	}
	
	public Boolean getViewPageShareButton()
	{
		return viewPageShareButton;
	}
	
	public void setViewPageShareButton(Boolean viewPageShareButton)
	{
		this.viewPageShareButton = viewPageShareButton;
	}
}

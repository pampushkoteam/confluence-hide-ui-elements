AJS.toInit(function () {
	var remoteUser = AJS.params.remoteUser;
	//Boolean(str) = check for an empty
	//returns false for null,undefined,0,000,"",false.
	//returns true for string "0" and whitespace " ".
	if (Boolean(remoteUser)) {
		var permission = isShareButtonEnabled();
		if (permission) {
			AJS.$('.gliffy-webpanel-footer').show();
			AJS.$('.aui-button#gliffy-dashboard-link').show();
			AJS.$('.aui-message.shadowed.closeable').show();
			AJS.$('.license.license-eval').show();

		} else {
			AJS.$('.gliffy-webpanel-footer').hide();
			AJS.$('.aui-button#gliffy-dashboard-link').hide();
			AJS.$('.aui-message.shadowed.closeable').hide();
			AJS.$('.license.license-eval').hide();
		}
	}
});


// http://localhost:8090/rest/interface_restriction/1.0/user/permission
function isShareButtonEnabled() {
	var permission;
	AJS.$.ajax({
		url: "/rest/interface_restriction/1.0/user/permission",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function (data) {
			permission = data.view_enabled;
		}
	});
	return permission;
}
